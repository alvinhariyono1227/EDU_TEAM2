//
//  ContentView.swift
//  EDU_TEAM2
//
//  Created by alvin hariyono on 11/10/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
        //test
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
